# Tiny Amazon :book:
> Tiny Amazon is a book management application with SvelteJS

## Get started

### Install the dependencies

```bash
npm install
```

### Start application

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). 

## Building and running in production mode

To create an optimised version of the app:

```bash
npm run build
```

## Developers
* **Romain HERAULT** - *Developer* - [r.herault](https://rherault.fr)
* **Florian Dubois** - *Developer*
