import PouchDB from 'pouchdb-browser';

let db = new PouchDB('tiny-amazon');

async function searchDocuments(query) {
  db.createIndex({
    index: {
      fields: ['title']
    }
  }).then(function (result) {
    db.find({
      selector: {title: {$regex: `.*${query}.*`}},
      fields: ['title', 'url', 'img', 'author'],
      sort: ['title']
    }).then(function(result) {
      console.log('searchDocuments result', result.docs);
      return result.docs;
    }).catch(function(error) {
      console.log(error);
    })

  }).catch(function (err) {
    console.log(err);
  });
  
}

async function addDocuments(book){
  db.post(book);
}

async function editDocuments(book){
  console.log('editDocuments', book);

  db.put({
    _id: book._id,
    title: book.title,
    price: book.price,
    author: book.author,
    img: book.img,
    url: book.url,
    _rev: book._rev
  }).then(function(response) {
    console.log(response);
  }).catch(function(err)  {
    console.log(err);
  });
}

async function deleteDocuments(book){
  db.remove(book);
}

export {searchDocuments, addDocuments, editDocuments, deleteDocuments};