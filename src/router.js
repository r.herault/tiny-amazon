import Home from './pages/Home.svelte';
import Author from './pages/Author.svelte';
import Add from './pages/Add.svelte';

import {writable} from 'svelte/store';

const router = {
  '/': Home,
  '/author': Author,
  '/add': Add,
}

export default router;

export const curRoute = writable('/');